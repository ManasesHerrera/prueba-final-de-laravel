-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-04-2017 a las 04:34:27
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pruebafinallaravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `direccion`, `email`, `password`, `estado`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Manuel Avila', 'Caracas', 'nose@nose.com', '$2y$10$SRZIUFR4IYGc/nB3ENDJ9.1PmP0Y63WPesPtia.HVFuMS1JDViAwK', 1, 'Activo', 'oJL2NZ3z3KzHpztgFqPBf05uROCkSDtfOMmdkhv3hJhp40C33mivcwReHWxl', '2017-03-29 03:04:25', '2017-04-01 00:42:01'),
(6, 'Manases ', 'Chile', 'dejame que recuerde', '$2y$10$r1EfLKOmBsIGOVXdpaO8W.MTLSF0N1ZDxJsWDAYF2.qm1GiSikhda', 1, 'Activo', NULL, '2017-03-29 03:15:22', '2017-03-29 16:21:20'),
(12, 'Fernando Bracamonte', 'Zuisa', 'np@np.com', '$2y$10$JsMtugFjrm/6HgCSl.aH6.m30m/ma2UiofGR29fP1O3jYrUszgrfG', 1, 'inActivo', 'ic3wQBcbXXXR5tXgfTA9wXEwD97OmFvluhCsdw3t7GALMI0hfUfdMZ7aN9p3', '2017-03-29 03:21:17', '2017-03-31 04:46:50'),
(13, 'Manases Herrera', 'altagracia de orituco, guarico ', 'herreramusik@gmail.com', '$2y$10$fTkf4JcvOZxWCzj09FWv3O1IYAu4Q/YDazz5mFRCT49H1kg7GmwRO', 1, 'Activo', 'KtNLeYhXdm8xv9Kc3Xidi1SZsvd643H9eKngwZRNwHZr5woRWriFRQHeO8gk', '2017-03-29 15:02:47', '2017-04-01 06:56:02'),
(14, 'Manuel Palacios', 'Ilustres proceres', 'no tiene correo', '$2y$10$lzX6axw0A/YWmJzNlG8Tq.ctWJ7Tmps9spsLph9rRZPSH3QSGy3sm', 1, 'Activo', NULL, '2017-03-29 16:17:28', '2017-03-29 16:17:53'),
(23, 'Lucia Buroz', 'no tengo ni idea de donde vive', 'tampocodesucorreo@mail.com', '$2y$10$6YormiC3zINa2z4SjyZYBeEmqw1DmESA/is7MFAnAO4llmWBUxa0m', 0, 'Inactivo', NULL, '2017-03-31 00:53:05', '2017-04-01 00:39:35'),
(24, 'Katerine Buroz', 'no se, ni idea', 'ejemplo@mail.com', '$2y$10$OHduO1Wdhxkvzf9/eKhkCe5pp6odNp2OGjLdRudfDBGP6zm9i/icG', 0, 'Activo', 'Li0sruHyokL9s5wEkOnZWVDhdPVZImsbUyFjOHgHsuA0PKaJqZSYXFBHTbG4', '2017-03-31 00:54:53', '2017-03-31 05:34:21'),
(25, 'Marianella Alvarado Rodriguez', 'Calle ilustres proceres al lado de mi refugio', 'alvaradomarianella15@gmail.com', '$2y$10$KkjODvQ1osq/P0/xpFE.z.j9wewsT5F2Dfp.SGxeO5TVhJ8SZ/Lia', 1, 'Activo', 'z0KXNJTdlKsqz4OAZPmpnARoXE6uwhE0km0ULTywW3LoGlnp1wAuEPa3ZM3t', '2017-03-31 01:58:05', '2017-04-01 07:02:55'),
(43, 'Leonel Carranza', 'Urbanizacion el Charco', 'mail@mail.com', '$2y$10$oKr.HjZ5wfNUoYC/HBH.j.dipk/Qe4ISjoLfaxTvLPblz7iLQNVTK', 1, 'Activo', 'TCVmgFNWAR3ifVtr3UzmsEriNy4GKPIf6L38jGRahSHGYEbaB3M9BSMnMbYP', '2017-04-01 00:43:04', '2017-04-01 07:02:02'),
(47, 'Sebastian Beomont', 'San juan ', 'herrera4e3@gmail.com', '$2y$10$upG/8TZwZBzEcwTlzfJsreBCjoec5eOY3lzLczLdFR2QXjfqULV7K', 0, 'Activo', NULL, '2017-04-01 06:44:24', '2017-04-01 06:44:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
