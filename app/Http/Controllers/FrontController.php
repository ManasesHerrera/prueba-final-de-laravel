<?php

namespace PruebaFinal\Http\Controllers;

use Illuminate\Http\Request;
use PruebaFinal\User;
use Session;
use Redirect;
use Mail;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
        User::create($request->all());//guardando toda la data
        //enviendo mensaje
        
        Mail::send('emails.bienvenida', $request->all(), function($msj){
            $msj->subject('Asunto');
        })->to($request['email']);
        //Esta parte si me comió mano. El tipo del video lo hace diferente, bueno el proposito no es mismo
        // Mail::send('emails.bienvenida', $request->all(), function($msj){
        //      $msj->subject('Asunto del correo');
        //      $msj->to('direccion de correo');
        // });
        //  el problema es que no puedo pasarle al metodo to() 
        //  la direccion de correo que ingrese el usuario para asi 
        //  mandarle el correo. De otro forma no habria problema.       
        //                
        //                                

        Session::flash('message', 'Usuario Registrado con éxito');
        return Redirect::to('/inicio/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
