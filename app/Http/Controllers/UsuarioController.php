<?php
namespace PruebaFinal\Http\Controllers;

date_default_timezone_set('America/Caracas'); 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PruebaFinal\User;
use Session;
use Redirect;
use Mail;

class UsuarioController extends Controller
{

    public function __construct(){
        //esto me estaba dando un error, asi que lo quité.
        //$this->middleware('auth');
        //$this->middleware('Admin');
       //$this->middleware('Admin', ['only'=>['create']]);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('administrador.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edicionDatos(){
        return view('administrador.updateDatesUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create($request->all());//guardando toda la data
        if ($request->ajax()) {
            //via ajax
            return Response()->json([ 
                'mensaje'=>'exito'
            ]);       
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::paginate(10);
        return view('administrador.list', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //recoge los datos del usuario y lo envia a la vista en forma de arreglo
        $user = User::find($id);
        return response()->json($user->toArray());
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            
            //buscando usuario
            $user = User::find($id);
            //actualizando
            $user->fill($request->all());
            //guardando
            $user->save();
            return response()->json([
                'mensaje' => 'listo'
            ]);
        }else{
            return "Error inesperado";
        }
    }

    public function logout(){
        Auth::logout();
        //if (Auth::user()->estado == 0) {
            $id = Auth::user()->id;
            DB::table('users')->where('id', $id)->update(['estado' => 1]);
        //}
        return Redirect::to('/');
        //return 'hola';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['mensaje'=>'listo']);
    }
}

 