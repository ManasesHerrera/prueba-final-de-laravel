<div class="modal fade" id="modal1"><!-- este div es el sombreado negro de la pantalla-->
	<div class="modal-dialog"> <!-- -->
		<div class="modal-content">
			<div class="modal-header"><!-- encabezado de la ventana -->
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Actualización de Datos</h4>
			</div>

			<div class="modal-body"> <!-- contenido de la ventana -->
				{!!Form::open(['method'=>'PUT', 'id'=>'formEdit'])!!}
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
				<input type="hidden" id="id">
				@include('forms.formCreate')
				{!!Form::close()!!}
			</div>

			<div class="modal-footer"><!-- pie de pagina de la ventana -->
				{!! link_to('#', $title="Actualizar", $attributes= ['id'=>'actualizar', 'class'=>'btn btn-info'], $secure = null  )  !!}
			</div>
		</div>
	</div>
</div>