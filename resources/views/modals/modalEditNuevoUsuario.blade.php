<div class="modal fade" id="modalEditNuevo"><!-- este div es el sombreado negro de la pantalla-->
	<div class="modal-dialog"> <!-- -->
		<div class="modal-content">
			<div class="modal-header"><!-- encabezado de la ventana -->
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Actualización de Datos</h4>
				<p>Para mayor seguridad de sus datos, le recomendamos actualizar su nombre y dirección</p>
				
			</div>
			<?php 
				$name = Auth::user()->name; 
				$dir =  Auth::user()->direccion; 
				$id =  Auth::user()->id; 
			?>	
			<div class="modal-body"> <!-- contenido de la ventana -->
				{!!Form::open(['id'=>'formEdit'])!!}
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
				<input type="hidden" id="id" value="<?php echo $id; ?>">
				<input type="hidden" name="estado" id="estado" value="1">
				
				<div class="row">
				  <div class="col-md-2 col-md-offset-1">
				    <div class="form-group">
				      {!!Form::label('name','Nombre')!!}<!--span style="color:red;">*</span-->
				    </div>
				  </div>
				  <div class="col-md-8">
				    <div class="form-group">
				    	<input type="text" name="name" value="<?php echo $name; ?>"  id="name" placeholder="Nombre Completo" class="form-control" required="required"/>
				    </div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-2 col-md-offset-1">
				    <div class="form-group">
				      {!!Form::label('direccion','Direcci&oacute;n')!!}<!--span style="color:red;">*</span-->
				    </div>
				  </div>
				  <div class="col-md-8">
				    <div class="form-group">
				    	<input type="text" value="<?php echo $dir; ?>" class="form-control" id="direccion" name="direccion" placeholder="Sector/Avenida/Calle/Ciudad" required="required"/>
				    </div>
				  </div>
				</div>

				{!!Form::close()!!}
			</div>

			<div class="modal-footer"><!-- pie de pagina de la ventana -->
				<div class="form-group pull-left">
					<label>
						<input type="checkbox" id="noMsjModal"> No mostrar de nuevo este mensaje. <small>Los datos no serán actualizados</small>
					</label>
				</div>	
						
				{!! link_to('#', $title="Actualizar", $attributes= ['id'=>'actua', 'class'=>'btn btn-info'], $secure = null  )  !!}
			</div>
		</div>
	</div>
</div>