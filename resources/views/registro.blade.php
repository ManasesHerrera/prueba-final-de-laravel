@extends('layouts.usuariosNormales')

	@section('content')

	<div class="container">
			
		<br><br>
		<h2 align="center">Formulario de Registro</h2>
		<br>

			
		<div style="width:70%;margin:10px auto;">
			@include('alerts.errors')
			@include('alerts.alert-success-multi')
		</div>
		
		<div class="alert alert-info" style="width:70%;margin:30px auto;" role="alert">
			Ingrese los datos solicitados y presione "Registrarme"
		</div>

		
		{!!Form::open([ 'route'=>'inicio.store', 'method'=>'POST'])!!}
			<!--incluyendo lines de formulario que se repiten en otros archivos-->
			<div class="row" align="center">
				<div class="col-md-9 col-md-offset-1" >
					@include('forms.formCreate')
					<input type="hidden" name="estado" value="0" >
					<input type="hidden" name="status" value="Activo" >


					<div class="form-group pull-center">
						{!!link_to('/', $title='Inicio de Sesión', $attributes = ['class'=>'btn btn-info'])!!}
						{!!Form::reset('Limpiar', ['class'=>'btn btn-danger'])!!}
						{!!Form::submit('Registrarme', ['class'=>'btn btn-primary'])!!}
					</div>
				</div>
			</div>
			

		{!!Form::close()!!} 

	</div>

	@endsection