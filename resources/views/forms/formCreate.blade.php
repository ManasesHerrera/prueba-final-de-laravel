<div class="row">
  <div class="col-md-2 col-md-offset-1">
    <div class="form-group">
      {!!Form::label('nombres','Nombre')!!}<!--span style="color:red;">*</span-->
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      {!!Form::text('name', null, [ 'id'=>'name', 'class'=>'form-control', 'placeholder'=>'Nombre Completo', 'required'=>'required' ])!!}                
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-md-offset-1">
    <div class="form-group">
      {!!Form::label('direccion','Direcci&oacute;n')!!}<!--span style="color:red;">*</span-->
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      {!!Form::text('direccion', null, [ 'id'=>'direccion', 'class'=>'form-control', 'placeholder'=>'Sector/Avenida/Calle/Ciudad', 'required'=>'required' ])!!}                
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-md-offset-1">
    <div class="form-group">
      {!!Form::label('correo','Correo')!!}<!--span style=":;float:right;color:red;">*</span-->
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      {!!Form::text('email', null, [ 'id'=>'email', 'class'=>'form-control', 'placeholder'=>'Ejemplo: correo@mail.com', 'required'=>'required' ])!!}                
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-md-offset-1">
    <div class="form-group">
      {!!Form::label('password','Contrase&ntilde;a')!!}<!--span style="color:red;">*</span-->
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      {!!Form::password('password',  ['class'=>'form-control', 'placeholder'=>'Contraseña', 'required'=>'required' ])!!}                
    </div>
  </div>
</div>
<!-- Datos adicionales usado para varios propositos -->
