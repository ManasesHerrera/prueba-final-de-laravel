@extends('layouts.admin')

	@section('content')
	 @include('modals.modalEdit')

		<div class="page-title">
      <div class="title_left">
        <h3>Listado de Usuarios</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Buscar</button>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Principal</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            @include('alerts.alert-success')

						<h2 align="center" >Listado de Usuarios </h2>
            <br><br>
            <table class="table table-striped table-hover">
              <thead>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Correo</th>
                <th>Operaciones</th>
              </thead>
              @foreach($users as $user)
              <tbody>
                <td>{{$user->name}}</td>
                <td>{{$user->direccion}}</td>
                <td>{{$user->email}}</td>
                <td>
                  <button  type="button"  value="{!!$user->id!!}" onClick="edit(this);" class="btn btn-primary btn-xs" title="Editar" data-toggle="tooltip" data-placement="bottom" data-original-title="Editar"> <span class="fa fa-edit"></span> </button>
                  <button type="button" value="{!!$user->id!!}" onClick="Eliminar(this);" class="btn btn-danger btn-xs" title="Eliminar" data-toggle="tooltip" data-placement="bottom" data-original-title="Eliminar"> <span class="fa fa-close"></span> </button>
                  @if ($user->status == 'Activo')
                    <button type="button" id="status" value="Inactivo" onClick="cambiarStatus({!!$user->id!!}, 0);" class="btn btn-default btn-xs" title="Status" data-toggle="tooltip" data-placement="bottom" data-original-title="Status"> <span class="fa fa-toggle-on"></span> </button>
                  @else
                    <button type="button" id="status" value="Activo" onClick="cambiarStatus({!!$user->id!!}, 1);" class="btn btn-default btn-xs" title="Status" data-toggle="tooltip" data-placement="bottom" data-original-title="Status"> <span class="fa fa-toggle-off"></span> </button>
                  @endif
                </td>
              </tbody>
              @endforeach
            </table>
            {!!$users->render()!!}
          </div>
        </div>
      </div>
    </div>

   @section('script')
      {!!Html::script('js/scriptCreate.js')!!} 
    @endsection

	@endsection