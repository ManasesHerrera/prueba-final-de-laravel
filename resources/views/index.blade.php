@extends('layouts.usuariosNormales')

@section('content');

	
	<div class="container">

		@include('alerts.errors')
		
		<h2 align="center">Página principal</h2>
		

		
		{!!Form::open([ 'route'=>'log.store', 'method'=>'POST'])!!}
			<!--incluyendo lines de formulario que se repiten en otros archivos-->
			<div class="row" align="center">
				<div class="col-md-6 col-md-offset-3" >
					<div class="form-group">
						{!!Form::text('email',null, ['placeholder'=>'Ingresa tu Correo' ,'class'=>'form-control','required'=>'required' ])!!}
					</div>
					<div class="form-group">
						{!!Form::password('password', ['placeholder'=>'Ingresa tu Contraseña' ,'class'=>'form-control','required'=>'required' ])!!}
					</div>
					<div class="form-group pull-right">
						{!!link_to('inicio/create', $title='Regístrate', $attributes = ['class'=>'btn btn-info'])!!}
						{!!Form::submit('Acceder', ['class'=>'btn btn-primary'])!!}
					</div>
				</div>
			</div>
			

		{!!Form::close()!!} 

	</div>

@stop