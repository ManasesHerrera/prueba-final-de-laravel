<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h3>Hola {!!$name!!}, que gusto que formes parte de nuestra comunidad</h3>
	<p>Estos son tus datos personales:</p>
	<p><b>Nombre:</b> {!!$name!!}</p>
	<p><b>Direccion:</b> {!!$direccion!!}</p>
	<p><b>Correo:</b> {!!$email!!}</p>
</body>
</html>