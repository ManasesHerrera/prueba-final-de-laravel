<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Prueba Final Laravel</title>
	 {!!Html::style('css/bootstrap.min.css')!!} 
	
</head>
<body>
	@yield('content')
	
	{!!Html::script('js/jquery.min.js')!!}
	{!!Html::script('js/bootstrap.min.js')!!}
</body>
</html>
