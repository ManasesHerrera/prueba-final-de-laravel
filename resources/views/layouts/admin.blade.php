<?php 


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Prueba final Laravel</title>
    
    <!-- Bootstrap -->
    {!!Html::style('css/bootstrap.min.css')!!}

    <!-- Font Awesome -->
    {!!Html::style('css/font-awesome/css/font-awesome.min.css')!!} 
    
    <!-- NProgress -->
    {!!Html::style('css/nprogress/nprogress.css')!!} 
    
    <!-- Custom Theme Style -->
    {!!Html::style('css/build/css/custom.min.css')!!} 

  </head>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              @if(Auth::user()->id == 13 ) 
              <a href="index.html" class="site_title"><i class="fa fa-cogs"></i> <span>Administrador</span></a>
              @else
              <a href="index.html" class="site_title"><i class="fa fa-user"></i> <span>Usuario</span></a>
              @endif

            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="img/imgDefault.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido</span>
                <h2>{!!Auth::user()->name!!}  </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="http://localhost/pruebaLaravel/public/usuario"><i class="fa fa-home"></i> Incio </a>
                  </li>
                @if(Auth::user()->id == 13 ) 
                  <li>
                    <a href="http://localhost/pruebaLaravel/public/usuario/create"><i class="fa fa-edit"></i> Registro de Usuarios </a>
                  </li>
                  <li>
                    <a href="http://localhost/pruebaLaravel/public/usuario/show"><i class="fa fa-list"></i> Listado de Usuarios </a>
                  </li>
                @else
                    <li>
                    <a href="http://localhost/pruebaLaravel/public/usuarios/{!!Auth::user()->id!!}/edit"><i class="fa fa-edit"></i> Editar Datos </a>
                  </li>
                @endif    
                </ul>
              </div>
              <div class="menu_section">
                
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
              <a  href="http://localhost/pruebaLaravel/public/cerrarSesion" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="fa fa-power-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/imgDefault.png" alt=""> {!!Auth::user()->name!!}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Perfil</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="http://localhost/pruebaLaravel/public/cerrarSesion"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
              
          <div class="">



            @yield('content')
            
          

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    
    <!-- Incluyento modal para edicion de nombre y direccion-->
    @include('modals.modalEditNuevoUsuario')
    
    <!-- jQuery -->
    {!!Html::script('js/jquery.min.js')!!} 
    
    <!-- Bootstrap -->
    {!!Html::script('js/bootstrap.min.js')!!} 
    
    <!-- FastClick -->
    {!!Html::script('js/fastclick.js')!!} 
    
    <!-- NProgress -->
    {!!Html::script('js/nprogress.js')!!} 
    
    <!-- Custom Theme Scripts -->
    {!!Html::script('js/custom.min.js')!!} 

    @section('script')
    @show

    @if (Auth::user()->estado == 0)
      {!!Html::script('js/scriptUsuarioNormal.js')!!} 
    @endif

  </body>
</html>
    
