$(document).ready(function(){
	$('#registro').click(function(){
		var datos = $('#formUserCreate').serialize();
		var route = 'http://localhost/pruebaLaravel/public/usuario';
		var token = $('#token').val();
		
		$.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN': token},
			type: 'POST',
			dataType: 'json',
			data: datos,
			success: function (){
				$('#sms-success').fadeIn();
			}
		});

	});
});

///////////////////////////////////////////
/////                                    //
// Seccion listar, editar y eliminar  // //
/////                                    //
///////////////////////////////////////////

function edit(btn){
	$('#modal1').modal('show');
	var route = "http://localhost/pruebaLaravel/public/usuario/"+btn.value+"/edit";
	$.get(route, function (res){
		$('#name').val(res.name);
		$('#direccion').val(res.direccion);
		$('#email').val(res.email);
		$('#id').val(res.id);
	});
}

$('#actualizar').click(function(){
	var val = $('#id').val();
	var dato = $('#formEdit').serialize();
	var route = "http://localhost/pruebaLaravel/public/usuario/"+val;
	var token = $('#token').val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: dato,
		success: function (){
			$('#modal1').modal('hide');
			$('#sms-success').fadeIn();
			$('#sms-success').append(". Debe actualizar la pagina para ver los cambios");
		}
	});
});


function Eliminar(btn){
	var route = "http://localhost/pruebaLaravel/public/usuario/"+btn.value;
	var token = $('#token').val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		success: function (){
			$('#sms-success').fadeIn();
			$('#sms-success').append(". Debe actualizar la pagina para ver los cambios");
		}
	});
}

function cambiarStatus(id, vari){
	var val = id;
	var value = null;
	//lo pongo en una sola linea para ahorrar espacio
	if (vari == 1) { value = "Activo"; }else{ value = "Inactivo"; };
	var dato = {'status': value};
	var route = "http://localhost/pruebaLaravel/public/usuario/"+val;
	var token = $('#token').val();
	
	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: dato,
		error: function(){
			alert('ha ocurrido un error inesperado, contacte al desarrollador del sistema');
		},
		success: function (){
			$('#sms-success').fadeIn();
			$('#sms-success').append(". Debe actualizar la pagina para ver los cambios");
		}
	});
}