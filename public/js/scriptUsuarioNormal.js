$(document).ready(function(){
  setTimeout(function(){
    $('#modalEditNuevo').modal('show');
  },1000);

  $('#actua').click(function(){
    var val = $('#id').val();
    var dato = $('#formEdit').serialize();
    var route = "http://localhost/pruebaLaravel/public/usuario/"+val;
    var token = $('#token').val();
    $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN': token},
      type: 'PUT',
      dataType: 'json',
      data: dato,
      error:  function(){
        alert('ha ocurrido un error inesperado');
      },
      success: function (){
        $('#modalEditNuevo').modal('hide');
        $('#sms-success').fadeIn();
        $('#sms-success').append(". Debe actualizar la pagina para ver los cambios");
      }
    });
  });
    
  $('#noMsjModal').click(function(){
    var val = $('#id').val();
    var dato = {'estado': 1};
    var route = "http://localhost/pruebaLaravel/public/usuario/"+val;
    var token = $('#token').val();
    $.ajax({
      url: route,
      headers: {'X-CSRF-TOKEN': token},
      type: 'PUT',
      dataType: 'json',
      data: dato,
      error:  function(){
        alert('ha ocurrido un error inesperado');
      },
      success: function (){
        $('#modalEditNuevo').modal('hide');
        $('#sms-success').fadeIn();
        $('#sms-success').append(". Debe actualizar la pagina para ver los cambios");
      }
    });
  });
});//cierre de funcion ready