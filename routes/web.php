<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
});

Route::get('inicio', 'FrontController@create');
Route::resource('inicio', 'FrontController');
Route::resource('usuario', 'UsuarioController');
Route::resource('log', 'LogController');
Route::resource('usuarios', 'UsernormalController');
Route::get('cerrarSesion', 'LogController@logout');


